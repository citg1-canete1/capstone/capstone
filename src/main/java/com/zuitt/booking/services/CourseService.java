package com.zuitt.booking.services;

import com.zuitt.booking.models.Course;
import com.zuitt.booking.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface CourseService {
    // Create a course
    void createCourse(Course course);

    // Get all courses
    Iterable<Course> getCourses();

    // Get course by id
    Optional<Course> getCourseById(int id);

    // Delete course by id
    ResponseEntity deleteCourse(int id);

    // Update course
    ResponseEntity updateCourse(int id, Course course);
/*
    // Enroll user to a course
    void enrollUser(int courseId, int userId);

    // Get all enrollments for a course
    Iterable<Integer> getEnrollments(int courseId);*/

    // Get active courses
    Iterable<Course> getActiveCourses();

    ResponseEntity<String> addStudentToCourse(int courseId, int studentId);

    ResponseEntity<String> removeStudentFromCourse(int courseId, int studentId);

    Iterable<User> getStudentsInCourse(int courseId);

    Boolean isCourseActive(int courseId);
}
