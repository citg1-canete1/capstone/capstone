package com.zuitt.booking.exceptions;

public class UserException extends Exception{
    public UserException(String message){
        super(message);
    }
}
