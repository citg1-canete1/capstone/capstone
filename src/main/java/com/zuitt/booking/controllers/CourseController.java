package com.zuitt.booking.controllers;

import com.zuitt.booking.models.Course;
import com.zuitt.booking.models.User;
import com.zuitt.booking.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @PostMapping
    public ResponseEntity<String> createCourse(@RequestBody Course course) {
        courseService.createCourse(course);
        return new ResponseEntity<>("Course created successfully", HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Iterable<Course>> getCourses() {
        Iterable<Course> courses = courseService.getCourses();
        return new ResponseEntity<>(courses, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Course> getCourseById(@PathVariable int id) {
        Optional<Course> course = courseService.getCourseById(id);
        return course.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCourse(@PathVariable int id) {
        courseService.deleteCourse(id);
        return new ResponseEntity<>("Course deleted successfully", HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateCourse(@PathVariable int id, @RequestBody Course course) {
        courseService.updateCourse(id, course);
        return new ResponseEntity<>("Course updated successfully", HttpStatus.OK);
    }

    @PostMapping("/{courseId}/students/{studentId}")
    public ResponseEntity<String> addStudentToCourse(@PathVariable int courseId, @PathVariable int studentId) {
        ResponseEntity<String> response = courseService.addStudentToCourse(courseId, studentId);
        return new ResponseEntity<>(response.getBody(), response.getStatusCode());
    }

    @DeleteMapping("/{courseId}/students/{studentId}")
    public ResponseEntity<String> removeStudentFromCourse(@PathVariable int courseId, @PathVariable int studentId) {
        ResponseEntity<String> response = courseService.removeStudentFromCourse(courseId, studentId);
        return new ResponseEntity<>(response.getBody(), response.getStatusCode());
    }

    @GetMapping("/{courseId}/students")
    public ResponseEntity<Iterable<User>> getStudentsInCourse(@PathVariable int courseId) {
        Iterable<User> users = courseService.getStudentsInCourse(courseId);
        return new ResponseEntity<>(users, HttpStatus.OK);
    }



    @GetMapping("/{courseId}/isActive")
    public ResponseEntity<Boolean> isCourseActive(@PathVariable int courseId) {
        Boolean isActive = courseService.isCourseActive(courseId);
        return new ResponseEntity<>(isActive, HttpStatus.OK);
    }

}